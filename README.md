# Beweb.Confinement

Voici un ensemble d'exercices réalisés durant le confinement du 15 Mars au 15 Avril 2020.

Ces exercices sont réalisés en Html, Css & JavaScript et sont mes vrais premières lignes de code !

Aucune modification ou factorisation n'a été effectué depuis la fin de ces exercices.
Je tiens à pouvoir comparer l'évolution de mon code à travers le temps.

Bonne balade !